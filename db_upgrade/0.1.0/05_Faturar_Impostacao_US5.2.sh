#!/bin/bash

#=======================================================================================
#                Accenture do Brasil Ltda.
# Autor - Rafael Lima e Pablo Costa
# Data - 20/09/2022
# Resumo - O script consiste em realizar o deploy de uma classe com campos customizados
# através do arquivo podl e executar 4 arquivos NAP utilizando os campos adicionados
#=======================================================================================

PROCESSNAME=05_Faturar_Impostacao_US5.2
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#funcões genéricas e carga de variaveis default
if [  ! -f "${PIN_HOME}/bin/util/functions.inc.sh" ]
then
	echo "ERROR: functions.inc.sh not found";
	exit -1;
fi
source ${PIN_HOME}/bin/util/functions.inc.sh

linfo "Starting: $PROCESSNAME $VERSION"

# prereq
checkEnv

isVM
if [ $? -eq 1 ]; then

    ### Realizar o parse dos custom fields
    #parse_custom_ops_fields.pl -L pcmc -I $SHELLDIR/../../include/c_flds.h -O $PIN_HOME/lib/c_flds.dat
    # O arquivo c_flds.dat gerado por esta função, em config-jobs, deve ser movimentado para brm-apps em algum momento
    fn_create_custom_fields_dat
    
    
    ### Verificando configuração do pin.conf
    retr=$(cat $PIN_HOME/sys/test/pin.conf | grep ^"- - ops_fields_extension_file" | wc -l 2>/dev/null)
    if [ $retr = 0 ]; then
       echo -e "\n- - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat" >> $PIN_HOME/sys/test/pin.conf
       echo "pin conf do test alterado"
    fi
 
    retr=$(cat $PIN_HOME/sys/cm/pin.conf | grep ^"- - ops_fields_extension_file" | wc -l 2>/dev/null)
    if [ $retr = 0 ]; then
       echo -e "\n- - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat" >> $PIN_HOME/sys/cm/pin.conf
       echo "pin conf do cm alterado"
    fi

    ### Reinicia o CM e o DM se estiver em ambiente de dev
    stopDMCM
    startDMCM
fi

### Realiza o pin_deploy da classe
linfo "Criar Classe config_c_dados_filiais no BRM"

podlApply config_c_service_lc_v0.podl config_c_lc_cod_servico_v0.podl c_rel_items_iss.podl svc_vivo_produtos.podl

### Executa os NAPs
testnap $CONFDIR/create_config_c_lc_cod_servico_v0.nap > success_lc_cod_servico_v0.log 2>erros_lc_cod_servico_v0.log
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: testnap create_config_c_lc_cod_servico_v0 return error $ retr"
     exit -1
else
      linfo "SUCESS: testnap loaded"
      rm  success_lc_cod_servico_v0.log erros_lc_cod_servico_v0.log
fi

testnap $CONFDIR/create_config_c_service_lc_v0.nap > success_service_lc_v0.log 2>erros_service_lc_v0.log
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: testnap create_config_c_service_lc_v0 return error $ retr"
     exit -1
else
      linfo "SUCESS: testnap loaded"
      rm success_service_lc_v0.log erros_service_lc_v0.log
fi

