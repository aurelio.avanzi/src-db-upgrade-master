#!/bin/bash

# nome e versão do script - obrigatório
PROCESSNAME=01_Faturar_Impostacao_US1.1
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [  ! -f "${PIN_HOME}/bin/util/functions.inc.sh" ]
then
	echo "ERROR: functions.inc.sh not found";
	exit -1;
fi
source ${PIN_HOME}/bin/util/functions.inc.sh

linfo "Starting: $PROCESSNAME $VERSION"


# prereq - DM e CM precisam estar rodando
stopDMCM

# prereq
checkEnv

isVM
if [ $? -eq 1 ]; then
      CMPINCONF=$PIN_HOME/sys/cm/pin.conf

      linfo "Applying currency change"
      #echo $CMPINCONF
      #linfo "Applying currency change 2"

      # Dever ser veirifcado a existência deste caminho de diretório destino em config-jobs e este ajuste deve ser recletido em src-connection-manager
      sed -i 's/fm_cust_pol currency 840/fm_cust_pol currency 986/' $CMPINCONF

      linfo "Currency change applied"

      startDMCM

      # prereq - DM e CM precisam estar rodando
      checkDMCMRunning

      linfo "Copying config_taxcodes_map.xsd - dockerfile function"

      # Dever ser veirifcado a existência deste caminho de diretório destino em config-jobs
      cp ${CONFDIR}/config_taxcodes_map.xsd ${PIN_HOME}/xsd/ 2>/dev/null

      retr=$?
      if [ $retr -ne 0 ]; then
      linfo "ERROR: Copy of config_taxcodes_map.xsd return error $retr"
      exit -1
      else
            linfo "SUCESS: Copy of config_taxcodes_map.xsd"
      fi

      linfo "Copying config_taxcodes_map.xml - dockerfile function"

      # Dever ser veirifcado a existência deste caminho de diretório destino em config-jobs
      cp ${CONFDIR}/config_taxcodes_map.xml ${PIN_HOME}/sys/data/config/ 2>/dev/null

      retr=$?
      if [ $retr -ne 0 ]; then
      linfo "ERROR: Copy of config_taxcodes_map.xml return error $retr"
      exit -1
      else
            linfo "SUCESS: Copy of config_taxcodes_map.xml"
      fi

      linfo "Copying config_tax_supplier.xml - dockerfile function"

      # Dever ser veirifcado a existência deste caminho de diretório destino em config-jobs
      cp ${CONFDIR}/config_tax_supplier.xml ${PIN_HOME}/sys/data/config/ 2>/dev/null

      retr=$?
      if [ $retr -ne 0 ]; then
      linfo "ERROR: Copy of config_tax_supplier.xml return error $retr"
      exit -1
      else
            linfo "SUCESS: Copy of config_tax_supplier.xml"
      fi
fi

cd $PIN_HOME/apps/load_config/

linfo "Carrying out tax clearance"

fn_load_config -v -r taxcodes_map

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: load_config return error $retr"
     exit -1
else
      linfo "SUCESS: load_config"
fi

linfo "Carrying out tax load"

fn_load_config -d -v ${PIN_HOME}/sys/data/config/config_taxcodes_map.xml

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: load_config return error $retr"
     exit -1
else
      linfo "SUCESS: load_config"
fi

fn_load_tax_supplier -d -v ${PIN_HOME}/sys/data/config/config_tax_supplier.xml

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: load_tax_supplier return error $retr"
     exit -1
else
      linfo "SUCESS: load_tax_supplier"
fi

linfo "Process finished!"
