#!/bin/bash

# nome e versão do script - obrigatório
PROCESSNAME=loadme
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [  ! -f "${PIN_HOME}/bin/util/functions.inc.sh" ]
then
	echo "ERROR: functions.inc.sh not found";
	exit -1;
fi
source ${PIN_HOME}/bin/util/functions.inc.sh

linfo "Starting: $PROCESSNAME $VERSION"

./01_Faturar_Impostacao_US1.1.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 01_Faturar_Impostacao_US1.1.sh $retr"
     exit -1
else
      linfo "SUCESS: 01_Faturar_Impostacao_US1.1.sh"
fi

./02_Faturar_Impostacao_US1.2.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 02_Faturar_Impostacao_US1.2.sh $retr"
     exit -1
else
      linfo "SUCESS: 02_Faturar_Impostacao_US1.2.sh"
fi

./03_Faturar_Impostacao_US3.2.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 03_Faturar_Impostacao_US3.2.sh $retr"
     exit -1
else
      linfo "SUCESS: 03_Faturar_Impostacao_US3.2.sh"
fi

./04_Faturar_Impostacao_US3.1.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 04_Faturar_Impostacao_US3.1.sh $retr"
     exit -1
else
      linfo "SUCESS: 04_Faturar_Impostacao_US3.1.sh"
fi

./05_Faturar_Impostacao_US5.2.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 05_Faturar_Impostacao_US5.2.sh $retr"
     exit -1
else
      linfo "SUCESS: 05_Faturar_Impostacao_US5.2.sh"
fi

./06_Faturar_Geral_US1.1.sh
retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: 06_Faturar_Geral_US1.1.sh $retr"
     exit -1
else
      linfo "SUCESS: 06_Faturar_Geral_US1.1.sh"
fi