#ifndef _C_FLDS_H
#define	_C_FLDS_H



#define C_FLD_FISCAL_UF			    PIN_MAKE_FLD(PIN_FLDT_STR, 50000)       // Eliel Andrade - US 3.1
#define C_FLD_RAZAO_SOCIAL		    PIN_MAKE_FLD(PIN_FLDT_STR, 50001)       // Eliel Andrade - US 3.1
#define C_FLD_CNPJ			        PIN_MAKE_FLD(PIN_FLDT_STR, 50002)       // Eliel Andrade - US 3.1
#define C_FLD_INS_ESTADUAL		    PIN_MAKE_FLD(PIN_FLDT_STR, 50003)       // Eliel Andrade - US 3.1
#define C_FLD_INS_MUNICIPAL		    PIN_MAKE_FLD(PIN_FLDT_STR, 50004)       // Eliel Andrade - US 3.1
#define C_FLD_END_LOGRADOURO		PIN_MAKE_FLD(PIN_FLDT_STR, 50005)       // Eliel Andrade - US 3.1
#define C_FLD_END_BAIRRO		    PIN_MAKE_FLD(PIN_FLDT_STR, 50006)       // Eliel Andrade - US 3.1
#define C_FLD_END_CIDADE		    PIN_MAKE_FLD(PIN_FLDT_STR, 50007)       // Eliel Andrade - US 3.1
#define C_FLD_END_CEP			    PIN_MAKE_FLD(PIN_FLDT_STR, 50008)       // Eliel Andrade - US 3.1
#define C_FLD_FLAG_FATURAMENTO		PIN_MAKE_FLD(PIN_FLDT_STR, 50009)       // Eliel Andrade - US 3.1
#define C_FLD_CNAE			        PIN_MAKE_FLD(PIN_FLDT_STR, 50010)       // Eliel Andrade - US 3.1
#define C_FLD_REGIME_ESPECIAL		PIN_MAKE_FLD(PIN_FLDT_STR, 50011)       // Eliel Andrade - US 3.1
#define C_FLD_INFO_DADOS_FILIAIS	PIN_MAKE_FLD(PIN_FLDT_ARRAY, 50012)     // Eliel Andrade - US 3.1
#define C_FLD_COD_UNIDADE		    PIN_MAKE_FLD(PIN_FLDT_STR, 50013)       // Eliel Andrade - US 3.1
#define C_FLD_TIPO_IMPOSTO		    PIN_MAKE_FLD(PIN_FLDT_STR, 50014)       // Eliel Andrade - US 3.1
#define C_FLD_FLAG_COB			    PIN_MAKE_FLD(PIN_FLDT_STR, 50015)       // Eliel Andrade - US 3.1

#define C_FLD_EXTRACTION_ID			PIN_MAKE_FLD(PIN_FLDT_INT, 50016)       // Sergio Andrade - Faturar Impostação - US 1.2 - Cálculo de Impostos
#define C_FLD_FILE_NAME			    PIN_MAKE_FLD(PIN_FLDT_STR, 50017)       // Sergio Andrade - Faturar Impostação - US 1.2 - Cálculo de Impostos
#define C_FLD_MODULE_NAME			PIN_MAKE_FLD(PIN_FLDT_STR, 50018)       // Sergio Andrade - Faturar Impostação - US 1.2 - Cálculo de Impostos

#define C_FLD_COMMISSION_ORIGIN		PIN_MAKE_FLD(PIN_FLDT_STR, 50019)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_COMMISSION_TAX		PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50020)   // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_COMMISSION_VALUE		PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50021)   // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_AGENT_ID			    PIN_MAKE_FLD(PIN_FLDT_INT, 50022)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_NF_GENERATED			PIN_MAKE_FLD(PIN_FLDT_STR, 50023)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_NF_NO			        PIN_MAKE_FLD(PIN_FLDT_STR, 50024)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_SYSTEMIC_ERRORS		PIN_MAKE_FLD(PIN_FLDT_INT, 50025)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_INTERNATIONAL			PIN_MAKE_FLD(PIN_FLDT_INT, 50026)       // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate
#define C_FLD_VALUE_LIQUI_INTER		PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50027)   // Isaque Porto/Bruno Santos - Faturar Impostação - US 3.2 - c_bill generate

// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_nota_fiscal generate
#define C_FLD_NUMERO_NF             PIN_MAKE_FLD(PIN_FLDT_STR, 50028)       
#define C_FLD_NF_CREATE_DT          PIN_MAKE_FLD(PIN_FLDT_TSTAMP, 50029)    
#define C_FLD_NF_MODELO             PIN_MAKE_FLD(PIN_FLDT_STR, 50030)       
#define C_FLD_NF_HASHCODE           PIN_MAKE_FLD(PIN_FLDT_STR, 50031)       
#define C_FLD_VALOR_TOTAL           PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50032)   
#define C_FLD_NF_BASE_CALCULO       PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50033)   
#define C_FLD_NF_VALOR_IMPOSTO      PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50034)   
#define C_FLD_NF_VALOR_ISENTO       PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50035)   
#define C_FLD_NF_VALOR_OUTROS       PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50036)   
#define C_FLD_NF_SITUACAO           PIN_MAKE_FLD(PIN_FLDT_STR, 50037)       
#define C_FLD_NF_ANOMES_REF         PIN_MAKE_FLD(PIN_FLDT_INT, 50038)       
#define C_FLD_TELEFONE              PIN_MAKE_FLD(PIN_FLDT_STR, 50039)       
#define C_FLD_TIPO_CLIENTE          PIN_MAKE_FLD(PIN_FLDT_INT, 50040)       
#define C_FLD_CNPJ_EMITENTE         PIN_MAKE_FLD(PIN_FLDT_STR, 50041)       
#define C_FLD_INFO_ADICIONAL        PIN_MAKE_FLD(PIN_FLDT_STR, 50042)       
#define C_FLD_NF_HASHCODE_2         PIN_MAKE_FLD(PIN_FLDT_STR, 50043)       
#define C_FLD_CLASSE_CONSUMO        PIN_MAKE_FLD(PIN_FLDT_INT, 50044)       
#define C_FLD_COD_CFOP              PIN_MAKE_FLD(PIN_FLDT_STR, 50045)       
#define C_FLD_ORDEM_ITEM            PIN_MAKE_FLD(PIN_FLDT_STR, 50046)       
#define C_FLD_SERVICE_TYPE          PIN_MAKE_FLD(PIN_FLDT_STR, 50047)       
#define C_FLD_CLASSIF_ITEM          PIN_MAKE_FLD(PIN_FLDT_INT, 50048)       
#define C_FLD_FISCAL_ITEM_UNIDADE   PIN_MAKE_FLD(PIN_FLDT_STR, 50049)       
#define C_FLD_FISCAL_ITEM_TOTAL     PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50050)       
#define C_FLD_FISCAL_ITEM_DESCONTO  PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50051)       
#define C_FLD_FISCAL_ITEM_DESPESAS_ACESSORIAS   PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50052)       
#define C_FLD_FISCAL_NF_ALIQUOTA            PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50053)           
#define C_FLD_FISCAL_QTD_ITEMS              PIN_MAKE_FLD(PIN_FLDT_INT, 50054)           
#define C_FLD_FISCAL_NF_ALIQUOTA_PIS        PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50055)           
#define C_FLD_NF_VALOR_IMPOSTO_PIS          PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50056)
#define C_FLD_FISCAL_NF_ALIQUOTA_COFINS     PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50057)
#define C_FLD_NF_VALOR_IMPOSTO_COFINS       PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50058)
#define C_FLD_TIPO_ISENCAO                  PIN_MAKE_FLD(PIN_FLDT_INT, 50059)
#define C_FLD_END_NUMERO                    PIN_MAKE_FLD(PIN_FLDT_INT, 50060)
#define C_FLD_END_COMPLEMENTO               PIN_MAKE_FLD(PIN_FLDT_STR, 50061)
#define C_FLD_TELEFONE_CONT                 PIN_MAKE_FLD(PIN_FLDT_STR, 50062)
#define C_FLD_NF_SERIE                      PIN_MAKE_FLD(PIN_FLDT_STR, 50063)
#define C_FLD_CITY_CODE                     PIN_MAKE_FLD(PIN_FLDT_INT, 50064)
#define C_FLD_NUM_CONTRATO                  PIN_MAKE_FLD(PIN_FLDT_STR, 50065)
#define C_FLD_FLAG_JUDICIAL                 PIN_MAKE_FLD(PIN_FLDT_STR, 50066)
// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_nota_fiscal generate

// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_items generate
#define C_FLD_ICMS_TAX                  PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50067)
#define C_FLD_PIS_TAX                   PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50068)
#define C_FLD_COFINS_TAX                PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50069)
#define C_FLD_CPBR_TAX                  PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50070)
#define C_FLD_STATUS_DESC               PIN_MAKE_FLD(PIN_FLDT_STR, 50071)
#define C_FLD_STATUS_ID                 PIN_MAKE_FLD(PIN_FLDT_ENUM, 50072)
#define C_FLD_ICMS_TAX_ID               PIN_MAKE_FLD(PIN_FLDT_INT, 50073)
#define C_FLD_PIS_TAX_ID                PIN_MAKE_FLD(PIN_FLDT_INT, 50074)
#define C_FLD_COFINS_TAX_ID             PIN_MAKE_FLD(PIN_FLDT_INT, 50075)
#define C_FLD_ISS_TAX_ID                PIN_MAKE_FLD(PIN_FLDT_INT, 50076)
#define C_FLD_FISCAL_RECEIPT_OBJ        PIN_MAKE_FLD(PIN_FLDT_POID, 50077)
#define C_FLD_COMPANY_CODE              PIN_MAKE_FLD(PIN_FLDT_STR, 50078)
#define C_FLD_COMPANY_GRP_CODE          PIN_MAKE_FLD(PIN_FLDT_STR, 50079)
#define C_FLD_COB_ENABLED               PIN_MAKE_FLD(PIN_FLDT_STR, 50080)
#define C_FLD_ACC_STATUS_FLAGS          PIN_MAKE_FLD(PIN_FLDT_INT, 50081)
#define C_FLD_NUMERO_SEQUENCIAL         PIN_MAKE_FLD(PIN_FLDT_STR, 50082)
#define C_FLD_ID_ITEM                   PIN_MAKE_FLD(PIN_FLDT_STR, 50083)
// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_items generate

// Rafael Lima/Pablo Costa - Faturar Impostação - US 5.2 c_rel_items.podl
#define C_FLD_EMISSORA                  PIN_MAKE_FLD(PIN_FLDT_STR, 50084)
#define C_FLD_ESTADO                    PIN_MAKE_FLD(PIN_FLDT_STR, 50085)
#define C_FLD_MUNICIPIO                 PIN_MAKE_FLD(PIN_FLDT_STR, 50086)
#define C_FLD_CNPJ_CLIENTE              PIN_MAKE_FLD(PIN_FLDT_STR, 50087)
#define C_FLD_NOME_CLIENTE              PIN_MAKE_FLD(PIN_FLDT_STR, 50088)
#define C_FLD_ENDERECO_CLIENTE          PIN_MAKE_FLD(PIN_FLDT_STR, 50089)
#define C_FLD_PRODUTO                   PIN_MAKE_FLD(PIN_FLDT_STR, 50090)
#define C_FLD_CONCEITO_FAT              PIN_MAKE_FLD(PIN_FLDT_STR, 50091)
#define C_FLD_STR_SERVICO               PIN_MAKE_FLD(PIN_FLDT_STR, 50092)
#define C_FLD_EMAIL                     PIN_MAKE_FLD(PIN_FLDT_STR, 50093)
#define C_FLD_RETENCAO                  PIN_MAKE_FLD(PIN_FLDT_STR, 50094)
#define C_FLD_IRRF                      PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50095)
#define C_FLD_PIS                       PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50096)
#define C_FLD_COFINS                    PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50097)
#define C_FLD_CSLL                      PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50098)
#define C_FLD_LC_COD_SERVICO            PIN_MAKE_FLD(PIN_FLDT_ARRAY, 50099)
#define C_FLD_SERVICE_LC                PIN_MAKE_FLD(PIN_FLDT_ARRAY, 50100)
#define C_FLD_DESCRIPTION               PIN_MAKE_FLD(PIN_FLDT_STR, 50101)
// Rafael Lima/Pablo Costa - Faturar Impostação - US 5.2 c_rel_items.podl

#define C_FLD_INFO_DADOS_OPERADORAS	    PIN_MAKE_FLD(PIN_FLDT_ARRAY, 50102)     // Sergio Andrade - Faturar Impostação - US 3.1 - c_dados_filiais -> c_dados_operadoras
#define C_FLD_COD_CSP                   PIN_MAKE_FLD(PIN_FLDT_INT, 50103)       // Sergio Andrade - Faturar Impostação - US 3.1 - c_dados_filiais -> c_dados_operadoras
#define C_FLD_NF_SUB_SERIE              PIN_MAKE_FLD(PIN_FLDT_STR, 50105)       // Sergio Andrade - Faturar Impostação - US 3.1 - c_dados_filiais -> c_dados_operadoras

// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_items generate
#define C_FLD_ISS_TAX                   PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50106)
#define C_FLD_FCP_TAX                   PIN_MAKE_FLD(PIN_FLDT_DECIMAL, 50107)
#define C_FLD_NOTA_FISCAL_OBJ           PIN_MAKE_FLD(PIN_FLDT_POID, 50108)
#define C_FLD_REL_ISS_GENERATED         PIN_MAKE_FLD(PIN_FLDT_INT, 50109)
// Isaque Porto/Bruno Santos - Faturar Impostação - US 3.3 - c_items generate

#endif
# /* !_C_FLDS_H */
