#!/bin/bash

#=======================================================================================
#                Accenture do Brasil Ltda.
# Autor - Eliel dos teclados e Aur�lio Avanzi
# Data - 22/09/2022
# Resumo - O script consiste em realizar o deploy de duas classes com campos CORE do BRM
# atrav�s do arquivo podl e atualizar arquivos XML de par�metros
#=======================================================================================

PROCESSNAME=06_Faturar_Geral_US1.1
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PARAMSDIR=/home/pin/brm/BRM/sys/data/config
SRCDIR=/home/pin/brm/BRM/source/sys

source /home/pin/brm/BRM/bin/util/functions.inc.sh


# Mover os c�digos-fonte e as bibliotecas customizadas
cp $CONFDIR/fm_bill_pol/fm_bill_pol_calc_pymt_due_t.c $SRCDIR/fm_bill_pol/fm_bill_pol_calc_pymt_due_t.c
if [ $? -eq 0 ]; then
	echo "fm_bill_pol_calc_pymt_due_t source moved"
fi
cp $CONFDIR/fm_bill_pol/fm_bill_pol_config.c $SRCDIR/fm_bill_pol/fm_bill_pol_config.c
if [ $? -eq 0 ]; then
	echo "fm_bill_config moved"
fi

cp $CONFDIR/c_ops.h $PIN_HOME/include/c_ops.h
if [ $? -eq 0 ]; then
	echo "c_ops.h moved"
fi

# Mover a pasta fm_c_bill
cp -r $CONFDIR/fm_c_bill $SRCDIR/fm_c_bill
if [ $? -eq 0 ]; then
	echo "fm_c_bill source moved"
fi

cp $CONFDIR/fm_c_bill_custom.so /home/pin/brm/BRM/lib/fm_c_bill_custom.so
if [ $? -eq 0 ]; then
	echo "fm_c_bill_custom.so moved"
fi

cp $CONFDIR/fm_bill_pol_custom.so /home/pin/brm/BRM/lib/fm_bill_pol_custom.so
if [ $? -eq 0 ]; then
	echo "fm_bill_pol_custom.so moved"
fi

cp $CONFDIR/c_ops.dat /home/pin/brm/BRM/lib/c_ops.dat
if [ $? -eq 0 ]; then
	echo "c_ops.dat moved"
fi

# Incluindo as bibliotecas dos policy opcodes
isVM
if [ $? -eq 1 ]; then
	cat $PIN_HOME/sys/cm/pin.conf | grep "cm fm_module ${PIN_HOME}/lib/fm_c_bill_custom${LIBRARYEXTENSION} fm_c_bill_custom_config fm_c_bill_init pin" > /dev/null
	
	if [ $? -eq 1 ]; then
		echo - cm fm_module ${PIN_HOME}/lib/fm_c_bill_custom${LIBRARYEXTENSION} fm_c_bill_custom_config fm_c_bill_init pin>> $PIN_HOME/sys/cm/pin.conf
		echo "fm_c_bill_custom added"
	fi
	
	cat $PIN_HOME/sys/cm/pin.conf | grep "cm fm_module ${PIN_HOME}/lib/fm_bill_pol_custom${LIBRARYEXTENSION} fm_bill_pol_custom_config fm_bill_pol_init pin" > /dev/null
	
	if [ $? -eq 1 ]; then
		echo - cm fm_module ${PIN_HOME}/lib/fm_bill_pol_custom${LIBRARYEXTENSION} fm_bill_pol_custom_config fm_bill_pol_init pin>> $PIN_HOME/sys/cm/pin.conf
		echo "fm_bill_pol_custom added"
	fi
	
	cat $PIN_HOME/sys/cm/pin.conf | grep "cm_cache fm_cust_pol_paymentterm_cache 256, 40960, 64" > /dev/null
	
	if [ $? -eq 1 ]; then
		echo "- cm_cache fm_cust_pol_paymentterm_cache 256, 40960, 64">> $PIN_HOME/sys/cm/pin.conf
		echo "payment term cache added"
	fi

	cat $PIN_HOME/sys/cm/pin.conf | grep "ops_fields_extension_file ${PIN_HOME}/lib/c_ops.dat" > /dev/null
	
	if [ $? -eq 1 ]; then
		echo "- - ops_fields_extension_file ${PIN_HOME}/lib/c_ops.dat">> $PIN_HOME/sys/cm/pin.conf
		echo "sys/cm c_ops.dat added"
	fi
	
	cat $PIN_HOME/sys/test/pin.conf | grep "ops_fields_extension_file ${PIN_HOME}/lib/c_ops.dat" > /dev/null
	
	if [ $? -eq 1 ]; then
		echo "- - ops_fields_extension_file ${PIN_HOME}/lib/c_ops.dat">> $PIN_HOME/sys/test/pin.conf
		echo "sys/test c_ops.dat added"
	fi
	
	stopDMCM
	startDMCM
fi


# Deploy dos arquivos XML de configura��o

cd $PARAMSDIR
find . -type f -name "bus_params_billing.xml" | xargs sed -i '/ConfigBillingDelay/s/1/0/'
find . -type f -name "bus_params_billing.xml" | xargs sed -i '/ShortCycle/s/disabled/enabled/'
pin_bus_params bus_params_billing.xml

find . -type f -name "bus_params_Invoicing.xml" | xargs sed -i '/InvoiceStorageType/s/0/1/'
pin_bus_params bus_params_Invoicing.xml

cp $CONFDIR/pin_payment_term.xml pin_payment_term.xml
if [ $? -eq 0 ]; then
	echo "pin_payment_term.xml moved"
fi
load_pin_payment_term pin_payment_term.xml

cp $CONFDIR/pin_billing_segment.xml pin_billing_segment.xml
if [ $? -eq 0 ]; then
	echo "pin_billing_segment.xml moved"
fi
load_pin_billing_segment pin_billing_segment.xml


# Aplica os PODLs das classes customizadas

cd $PIN_HOME/sys/test/
pin_deploy replace $CONFDIR/c_item_trial.podl
pin_deploy replace $CONFDIR/c_bill_trial.podl

isVM
if [ $? -eq 1 ]; then
	stopDMCM
	startDMCM
fi







