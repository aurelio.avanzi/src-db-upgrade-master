#!/bin/bash

# nome e versão do script - obrigatório
PROCESSNAME=02_Faturar_Impostacao_US1.2
VERSION="0.1"
SHELLDIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#funcões genéricas e carga de variaveis default
if [  ! -f "${PIN_HOME}/bin/util/functions.inc.sh" ]
then
	echo "ERROR: functions.inc.sh not found";
	exit -1;
fi
source ${PIN_HOME}/bin/util/functions.inc.sh

linfo "Starting: $PROCESSNAME $VERSION"

# prereq
checkEnv

isVM
if [ $? -eq 1 ]; then
   isCMDMRunning
   retr=$?
   if [ $retr -ne 0 ]
   then
      linfo "${PROCESSNAME}: WARNING: DM OR CM is not running"
      startDMCM
   fi
      linfo "${PROCESSNAME}: DM AND CM is Running"

   # prereq - DM e CM precisam estar rodando
   checkDMCMRunning

   ### Realizar o parse dos custom fields
   #parse_custom_ops_fields.pl -L pcmc -I $SHELLDIR/../../include/c_flds.h -O $PIN_HOME/lib/c_flds.dat
   # O arquivo c_flds.dat gerado por esta função, em config-jobs, deve ser movimentado para brm-apps em algum momento
   fn_create_custom_fields_dat

   ### Verificando configuração do pin.conf
   retr=$(cat $PIN_HOME/sys/test/pin.conf | grep ^"- - ops_fields_extension_file" | wc -l 2>/dev/null)
   if [ $retr = 0 ]; then
      echo -e "\n- - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat" >> $PIN_HOME/sys/test/pin.conf
      echo "pin conf do test alterado"
   fi

   retr=$(cat $PIN_HOME/sys/cm/pin.conf | grep ^"- - ops_fields_extension_file" | wc -l 2>/dev/null)
   if [ $retr = 0 ]; then
      echo -e "\n- - ops_fields_extension_file ${PIN_HOME}/lib/c_flds.dat" >> $PIN_HOME/sys/cm/pin.conf
      echo "pin conf do cm alterado"
   fi

   # prereq - DM e CM precisam estar rodando
   stopDMCM
   startDMCM
fi

#aplicado podls
linfo "Criar Classe c_batch_process_results no BRM"

podlApply c_batch_process_results.podl

retr=$?
if [ $retr -ne 0 ]; then
     linfo "ERROR: podlApply return error $ retr"
     exit -1
else
      linfo "SUCESS: podlApply loaded"
fi

exit 0
