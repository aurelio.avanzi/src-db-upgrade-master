#===============================================================================
#/// @addtogroup DATAMODEL
#/// @{
#/// @file
#/// @brief Definition of /config/c_lc_cod_servico storable class.
#/// @copyright Copyright (c) 2021, Accenture. All rights reserved.
#/// @}
#===============================================================================
# $Id: config_c_lc_cod_servico.podl  2022-09-19 HH:MM:SSZ $
#===============================================================================

#=======================================
#  Custom Fields for AC Config Items
#=======================================

ARRAY C_FLD_LC_COD_SERVICO {
	
	ID = 50099;
}

STRING C_FLD_MUNICIPIO {
	
	ID = 50086;
}

STRING C_FLD_ID_ITEM {
	
	ID = 50083;
}

STRING C_FLD_STR_SERVICO {
	
	ID = 50092;
}

#=======================================
#  Storable Class /config/c_lc_cod_servico
#=======================================

STORABLE CLASS /config/c_lc_cod_servico {

    LABEL = "$Rev: 1704 $";
	READ_ACCESS = "BrandLineage";
	WRITE_ACCESS = "BrandLineage";
	DESCR = "Bill extended class.";
	IS_PARTITIONED = "0";
	
	#=======================================
	#  Fields
	#=======================================
	
	ARRAY C_FLD_LC_COD_SERVICO {

		DESCR = "This contains configurable information for each parameter.";
		ORDER = 0;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
		
		
		STRING C_FLD_MUNICIPIO {
			DESCR = "Município de prestação do serviço.";
			ORDER = 0;
			LENGTH = 255;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}   


		STRING C_FLD_ID_ITEM {
			DESCR = "Código atribuído do Item da Nota Fiscal.";
			ORDER = 0;
			LENGTH = 13;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}
		
		STRING C_FLD_STR_SERVICO {
	
			DESCR = "Codigo de Servico na Vivo";
			ORDER = 0;
			LENGTH = 255;
			CREATE = Optional;
			MODIFY = Writeable;
			AUDITABLE = 0;
			ENCRYPTABLE = 0;
			SERIALIZABLE = 0;
		}		
	}
}

#=======================================
#  Storable Class /config/c_lc_cod_servico
#=======================================

STORABLE CLASS /config/c_lc_cod_servico IMPLEMENTATION ORACLE7 {


	#===================
	#  Fields 
	#===================

	ARRAY C_FLD_LC_COD_SERVICO  {

		SQL_TABLE = "config_c_lc_cod_servico_t";

		#===================
		#  Fields 
		#===================
		
		STRING C_FLD_MUNICIPIO {
		
			SQL_COLUMN = "MUNICIPIO";
		}

		STRING C_FLD_ID_ITEM {
		
			SQL_COLUMN = "ITEM_LC";
		}
		
		STRING C_FLD_STR_SERVICO {
	
			SQL_COLUMN = "COD_SERVICO";
		} 			
	}
}