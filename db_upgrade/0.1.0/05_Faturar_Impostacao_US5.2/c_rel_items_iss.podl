#===============================================================================
#/// @addtogroup DATAMODEL
#/// @{
#/// @file
#/// @brief Definition of /c_rel_items_iss storable class.
#/// @copyright Copyright (c) 2020, Accenture. All rights reserved.
#/// @}
#===============================================================================
# $Id: c_rel_items_iss.podl 193 2022-09-19 HH:MM:SSZ $
#===============================================================================


#=======================================
#  Field C_FLD_EMISSORA
#=======================================

STRING C_FLD_EMISSORA {

	ID = 50084;
}

#=======================================
#  Field C_FLD_ESTADO
#=======================================

STRING C_FLD_ESTADO {

	ID = 50085;
}

#=======================================
#  Field C_FLD_MUNICIPIO
#=======================================

STRING C_FLD_MUNICIPIO {

	ID = 50086;
}

#=======================================
#  Field C_FLD_CNPJ_CLIENTE
#=======================================

STRING C_FLD_CNPJ_CLIENTE {

	ID = 50087;
}

#=======================================
#  Field C_FLD_NOME_CLIENTE
#=======================================

STRING C_FLD_NOME_CLIENTE {
		
	ID = 50088;
}	

#=======================================
#  Field C_FLD_ENDERECO_CLIENTE
#=======================================

STRING C_FLD_ENDERECO_CLIENTE {
	
	ID = 50089;
}

#=======================================
#  Field PIN_FLD_BILLINFO_OBJ
#=======================================

POID PIN_FLD_BILLINFO_OBJ {

	ID = 7752;
}

#=======================================
#  Field C_FLD_PRODUTO
#=======================================

STRING C_FLD_PRODUTO {
	
	ID = 50090;
}

#=======================================
#  Field C_FLD_CONCEITO_FAT
#=======================================
STRING C_FLD_CONCEITO_FAT {

	ID = 50091;
}

#=======================================
#  Field C_FLD_STR_SERVICO
#=======================================
STRING C_FLD_STR_SERVICO {

	ID = 50092;
}

#=======================================
#  Field C_FLD_EMAIL
#=======================================
STRING C_FLD_EMAIL {

	ID = 50093;
}

#=======================================
#  Field PIN_FLD_ITEM_TOTAL
#=======================================

DECIMAL PIN_FLD_ITEM_TOTAL {

	ID = 784;
}

#=======================================
#  Field C_FLD_RETENCAO
#=======================================
STRING C_FLD_RETENCAO {

	ID = 50094;
}
	
#=======================================
#  Field C_FLD_IRRF
#=======================================

DECIMAL C_FLD_IRRF {

	ID = 50095;
}

#=======================================
#  Field C_FLD_PIS
#=======================================

DECIMAL C_FLD_PIS {

	ID = 50096;
}

#=======================================
#  Field C_FLD_COFINS
#=======================================

DECIMAL C_FLD_COFINS {

	ID = 50097;
}

#=======================================
#  Field C_FLD_CSLL
#=======================================

DECIMAL C_FLD_CSLL {

	ID = 50098;
}


#=======================================
#  Storable Class /c_rel_items_iss
#=======================================

STORABLE CLASS /c_rel_items_iss {

	LABEL = "$Rev: 1 $";
	READ_ACCESS = "BrandLineage";
	WRITE_ACCESS = "BrandLineage";
	DESCR = "Stores tax amount details ISS of the item. ";
	IS_PARTITIONED = "0";

	#===================
	#  Fields 
	#===================

	STRING C_FLD_EMISSORA {
	
		DESCR = "Empresa Emissora do ISS.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_ESTADO {
	
		DESCR = "UF da Empresa Emissora do ISS.";
		ORDER = 0;
		LENGTH = 2;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_MUNICIPIO {
	
		DESCR = "Município da Empresa Emissora do ISS.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_CNPJ_CLIENTE {
	
		DESCR = "CNPJ do Cliente.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_NOME_CLIENTE {
	
		DESCR = "Nome do Cliente.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_ENDERECO_CLIENTE {
	
		DESCR = "Endereço completo do Cliente.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	POID PIN_FLD_BILLINFO_OBJ {

		DESCR = "billinfo object: Identifica a conta cobrança do cliente";
		ORDER = 0;
		CREATE = Required;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
	
	STRING C_FLD_PRODUTO {
	
		DESCR = "Identificador do produto incidente.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_CONCEITO_FAT {
	
		DESCR = "Conceito faturável.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_STR_SERVICO {
	
		DESCR = "Código do serviço Vivo.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	STRING C_FLD_EMAIL {
	
		DESCR = "Endereço de email do cliente.";
		ORDER = 0;
		LENGTH = 255;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	DECIMAL PIN_FLD_ITEM_TOTAL {

		DESCR = "Total Value of the item.";
		ORDER = 0;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
	
	STRING C_FLD_RETENCAO {
	
		DESCR = "Indicador se cliente tem retenção (SIM) ou não tem retenção (NÃO).";
		ORDER = 0;
		LENGTH = 3;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 0;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}	
	
	DECIMAL C_FLD_IRRF {

		DESCR = "Total IRRF para retenção. ";
		ORDER = 0;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
	
	DECIMAL C_FLD_PIS {

		DESCR = "Total de PIS para retenção. ";
		ORDER = 0;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
	
	DECIMAL C_FLD_COFINS {

		DESCR = "Total de COFINS para retenção. ";
		ORDER = 0;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
	
	DECIMAL C_FLD_CSLL {

		DESCR = "Total de CSLL para retenção. ";
		ORDER = 0;
		CREATE = Optional;
		MODIFY = Writeable;
		AUDITABLE = 1;
		ENCRYPTABLE = 0;
		SERIALIZABLE = 0;
	}
}


#=======================================
#  Storable Class /c_rel_items_iss
#=======================================

STORABLE CLASS /c_rel_items_iss IMPLEMENTATION ORACLE7 {

	SQL_TABLE = "c_rel_items_iss_t";
	SQL_STORAGE = "tablespace pin00 storage (initial 1m next 1m maxextents unlimited pctincrease 0 ) ";

	#===================
	#  Fields 
	#===================

	STRING C_FLD_EMISSORA {

		SQL_COLUMN = "emissora";
	}

	STRING C_FLD_ESTADO {

		SQL_COLUMN = "estado";
	}

	STRING C_FLD_MUNICIPIO {

		SQL_COLUMN = "municipio";
	}

	STRING C_FLD_CNPJ_CLIENTE {

		SQL_COLUMN = "cnpj_cliente";
	}

	STRING C_FLD_NOME_CLIENTE {
		SQL_COLUMN = "nome_cliente";
	}
	
	STRING C_FLD_ENDERECO_CLIENTE {

		SQL_COLUMN = "endereco_cliente";
	}

	STRING C_FLD_PRODUTO {

		SQL_COLUMN = "produto";
	}

	STRING C_FLD_CONCEITO_FAT {

		SQL_COLUMN = "conceito_fat";
	}

	STRING C_FLD_STR_SERVICO {

		SQL_COLUMN = "cod_servico";
	}

	STRING C_FLD_EMAIL {

		SQL_COLUMN = "email";
	}

	DECIMAL PIN_FLD_ITEM_TOTAL {

		SQL_COLUMN = "item_total";
	}
	
	STRING C_FLD_RETENCAO {

		SQL_COLUMN = "retencao";
	}

	DECIMAL C_FLD_IRRF {

		SQL_COLUMN = "irrf";
	}

	DECIMAL C_FLD_PIS {

		SQL_COLUMN = "pis";
	}

	DECIMAL C_FLD_COFINS {

		SQL_COLUMN = "cofins";
	}

	DECIMAL C_FLD_CSLL {

		SQL_COLUMN = "csll";
	}
	POID PIN_FLD_BILLINFO_OBJ {

		SQL_COLUMN = "BILLINFO_OBJ";
	}
}
