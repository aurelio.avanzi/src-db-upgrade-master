/******************************************************************************
 *
* Copyright (c) 2011, 2013, Oracle and/or its affiliates. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid Oracle license or sublicense agreement.
 *
 *********************************************************************************/

#ifndef lint
static char Sccs_id[] = "@(#)$Id: fm_c_bill_custom_config.c /cgbubrm_7.5.0.portalbase/1 2015/11/27 05:32:52 nishahan Exp $";
#endif

#include <stdio.h>
#include "ops/bill.h"
#include "pcm.h"
#include "cm_fm.h"
#include "c_ops.h"

#ifdef WIN32
__declspec(dllexport) void * fm_c_bill_custom_config_func();
#endif


/********************************************************************
 * If you want to customize any of the op-codes commented below, you
 * need to uncomment it.
 *******************************************************************/
struct cm_fm_config fm_c_bill_custom_config[] = {
	
	//{ PCM_OP_C_BILL_SET_DUE_DOM, "op_c_bill_set_due_dom" },
	{ PCM_OP_C_BILL_DELAY_DUE_T, "op_c_bill_delay_due_t" },
	//{ PCM_OP_C_BILL_SET_PYMT_OFFSET, "op_c_bill_set_pymt_offset" },
	//{ PCM_OP_C_BILL_SET_PYMT_TERM, "op_c_bill_set_pymt_term" },
	{ PCM_OP_C_BILL_SET_PAYINFO, "op_c_bill_set_payinfo" },
	{ 0,    (char *)0 }
};

#ifdef WIN32
void *
fm_c_bill_custom_config_func()
{
  return ((void *) (fm_c_bill_custom_config));
}
#endif

