/*
 * Copyright (c) 2001, 2009, Oracle and/or its affiliates. 
 * All rights reserved. 
 *      This material is the confidential property of Oracle Corporation or its
 *      licensors and may be used, reproduced, stored or transmitted only in
 *      accordance with a valid Oracle license or sublicense agreement.
 */

#ifndef lint
static char Sccs_id[] = "@(#)$Id: fm_c_bill_delay_due_t.c /cgbubrm_7.5.0.portalbase/1 2015/11/27 05:32:43 nishahan Exp $";
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "pcm.h"
#include "ops/bill.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_cust.h"
#include "pin_bill.h"
#include "pin_pymt.h"
#include "pinlog.h"
#include "pin_os.h"
#include "fm_utils.h"
#include "fm_bill_utils.h"
#include "c_ops.h"


enum {DEFAULT = 0, TERM1, TERM2, TERM3, TERM4 = 1001, TERM5 = 1002, TERM6 = 1003};
enum {SUNDAY = 0, SATURDAY=6};

/**** Functions defined elsewhere ******/
extern time_t fm_utils_cycle_actgfuturet();
extern void fm_utils_get_calendar();

EXPORT_OP void
op_c_bill_delay_due_t(
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
fm_c_bill_delay_due_t(
	pcm_context_t	*ctxp,
	int32		flags,
	pin_flist_t	*in_flistp,
	pin_flist_t	**out_flistpp,
	pin_errbuf_t	*ebufp);

/*******************************************************************
 * Main routine for the PCM_OP_C_BILL_DELAY_DUE_T opcode
 *******************************************************************/
void
op_c_bill_delay_due_t(
	cm_nap_connection_t	*connp,
	int32			opcode,
	int32			flags,
	pin_flist_t		*i_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;
	void			*vp = NULL;

	if (PIN_ERR_IS_ERR(ebufp)) {
		return;
	}
	PIN_ERR_CLEAR_ERR(ebufp);

	*ret_flistpp = NULL;

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != PCM_OP_C_BILL_DELAY_DUE_T)
	{
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_bill_pol_delay_due", ebufp);
		return;
	}

	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_c_bill_delay_due_t: Input flist", i_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	fm_c_bill_delay_due_t(ctxp, flags, i_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (! PIN_ERR_IS_ERR(ebufp)) 
	{
		*ret_flistpp = PIN_FLIST_CREATE(ebufp);
		vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 0, ebufp);
		PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_POID, vp, ebufp);
		vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_BILL_OBJ, 0, ebufp);
		PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_BILL_OBJ, vp, ebufp);
		vp = PIN_FLIST_FLD_GET(r_flistp, PIN_FLD_DUE_T, 0, ebufp);
		PIN_FLIST_FLD_SET(*ret_flistpp, PIN_FLD_DUE_T, vp, ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_c_bill_delay_due_t: Output flist", *ret_flistpp);
	}

	PIN_FLIST_DESTROY_EX(&r_flistp, NULL);

	return;
}

/*******************************************************************
 * fm_c_bill_delay_due_t()
 *******************************************************************/
static void
fm_c_bill_delay_due_t(
	pcm_context_t	*ctxp,
	int32		flags,
	pin_flist_t	*i_flistp,
	pin_flist_t	**out_flistpp,
	pin_errbuf_t	*ebufp)
{
	pin_flist_t	*pi_i_flistp = NULL;
	pin_flist_t	*pi_o_flistp = NULL;
	pin_flist_t	*pay_offsetIn_flist = NULL;
	pin_flist_t	*pay_offsetOut_flist = NULL;
	pin_flist_t	*bi_i_flistp = NULL;
	pin_flist_t	*bi_o_flistp = NULL;
	pin_flist_t     *t_flistp = NULL;
	pin_flist_t     *r_flistp = NULL;		
	poid_t		*bi_pdp = NULL;
	poid_t		*acc_pdp = NULL;
	poid_t		*pi_pdp = NULL;
	void		*vp = NULL;
	time_t		due_t = (time_t)0;
	time_t		r_due_t = (time_t)0;
	time_t		*eff_tp = (time_t *)NULL;
	u_int		ptt;
	int32		due_date_adjust = 0;
	int32           *pay_typep = NULL;
	int		n_days = 0;
	int		due_dom = 0;
	int		payment_offset = 0;
	int		due_delay = 1728000;
	time_t		custom_duet = (time_t)0;
	int			offset = 0;

	if (PIN_ERR_IS_ERR(ebufp)) {
		return;
	}
	PIN_ERR_CLEAR_ERR(ebufp);	

	/***********************************************************
	* Create outgoing flist
	***********************************************************/
	
	//PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, "Criando Flist de saida");
	
	*out_flistpp = PIN_FLIST_CREATE(ebufp);

	acc_pdp = (poid_t *)PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_POID, 1, ebufp);
	if (!acc_pdp)
	{
		PIN_FLIST_LOG_ERR("fm_c_bill_delay_due_t: "
			"Mandatory value Account Poid missing in the input flist",
			ebufp);
		return;
	}
	
	vp = PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_PAYMENT_OFFSET, 1, ebufp);
	payment_offset = *(u_int *) vp;

	 
	/***************************************************************
	INICIO - US Faturar Geral 1.1 - Postergar vencimento de fatura
	***************************************************************/
	
	// Coletar offset da payinfo
	bi_pdp = (poid_t *)PIN_FLIST_FLD_GET(i_flistp, PIN_FLD_BILL_OBJ, 0, ebufp);
	if (!bi_pdp)
	{
		PIN_FLIST_LOG_ERR("fm_bill_pol_update_collect_date: "
			"Mandatory value BILL Poid missing in the input flist",
			ebufp);
		return;
	}

	/***********************************************************
	 * Get the payinfo object for a given BILL object
	 ***********************************************************/
	bi_i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_POID, (void *) bi_pdp, ebufp);
	PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_DUE_T, (void *) NULL, 
								ebufp);
	PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, bi_i_flistp, &bi_o_flistp, ebufp);
	
	
	vp = PIN_FLIST_FLD_GET(bi_o_flistp, PIN_FLD_DUE_T, 1, ebufp);
	if (vp) {
		custom_duet = *(time_t *) vp;
	}
	//custom_duet = (time_t *)PIN_FLIST_FLD_GET(bi_o_flistp, PIN_FLD_DUE_T, 1, ebufp);
	
	char tmp_custom_af[255];
	sprintf(tmp_custom_af, "Valor do custom_duet depois: %d", &custom_duet);
	PIN_ERR_LOG_MSG(PIN_ERR_LEVEL_WARNING, tmp_custom_af);
	
	
	// Postergação de vencimento
	fm_utils_add_n_days(payment_offset, &custom_duet);
					
	struct tm *tm = NULL;
	tm = localtime(&custom_duet);

	//If it's weekend, add business days
	if (tm->tm_wday == SATURDAY)
	{
		fm_utils_add_n_days(2, &custom_duet);
		tm = localtime(&custom_duet);
	}
	else if (tm->tm_wday == SUNDAY)
	{
		fm_utils_add_n_days(1, &custom_duet);
		tm = localtime(&custom_duet);
	}
			
	PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_DUE_T, (void *)&custom_duet, ebufp);
	
	
	/*
	tratamento de feriados (calendar)
	*/
	
	/************************************************************
	FIM - US Faturar Geral 1.1 - Cálculo de vencimento de faturas
	************************************************************/
	PIN_FLIST_DESTROY_EX(&bi_i_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&bi_o_flistp, NULL);
	
	
	bi_i_flistp = PIN_FLIST_CREATE(ebufp);
	PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_POID, (void *) bi_pdp, ebufp);
	PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_DUE_T, (time_t *) &custom_duet, ebufp);
	PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, bi_i_flistp, &bi_o_flistp, ebufp);


	/************************************************************
	* Set the PIN_FLD_DUE_T field in the config flist.
	************************************************************/
	PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_POID, (void *)acc_pdp, ebufp);
	PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_BILL_OBJ, (void *)bi_pdp, ebufp);
	//PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_DUE_T, (void *)&due_t, ebufp);
	
	/*********************************************************
	* Cleanup...
	*********************************************************/
	cleanup:

	PIN_FLIST_DESTROY_EX(&bi_i_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&bi_o_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&pi_i_flistp, NULL);
	PIN_FLIST_DESTROY_EX(&pi_o_flistp, NULL);

	/*********************************************************
        * Errors..?
        *********************************************************/
        if (PIN_ERR_IS_ERR(ebufp))
        {
                PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
                        "fm_c_bill_delay_due_t error", ebufp);
        }
	return;
}

