/*******************************************************************
 *
* Copyright (c) 1999, 2021, Oracle and/or its affiliates. All rights reserved.
 *
 *      This material is the confidential property of Oracle Corporation
 *      or its licensors and may be used, reproduced, stored or transmitted
 *      only in accordance with a valid Oracle license or sublicense agreement.
 *
 *******************************************************************/

#ifndef lint
static  char Sccs_Id[] = "@(#)$Id: fm_c_bill_set_payinfo.c /cgbubrm_mainbrm.portalbase/1 2017/08/29 06:53:38 crunkana Exp $";
#endif

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "pcm.h"
#include "ops/cust.h"
#include "pin_cust.h"
#include "cm_fm.h"
#include "pin_errs.h"
#include "pin_cust.h"
#include "pinlog.h"
#include "pin_bdfs.h"
#include "pin_pymt.h"
#include "pin_bill.h"
#include "pin_inv.h"        
#include "ops/pymt.h"        
#include "fm_utils.h"
#include "c_ops.h"        
extern char *fm_cust_pol_cm_dd_vendor;
        
EXPORT_OP void
op_c_bill_set_payinfo(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp);

static void
fm_c_bill_set_payinfo(
	pcm_context_t		*ctxp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
        pin_errbuf_t		*ebufp);

extern void fm_utils_get_tids();

/*******************************************************************
 * Main routine for the PCM_OP_C_BILL_SET_PAYINFO  command
 *******************************************************************/
void
op_c_bill_set_payinfo(
	cm_nap_connection_t	*connp,
	u_int			opcode,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**ret_flistpp,
	pin_errbuf_t		*ebufp)
{
	pcm_context_t		*ctxp = connp->dm_ctx;
	pin_flist_t		*r_flistp = NULL;

	/***********************************************************
	 * Null out results until we have some.
	 ***********************************************************/
	*ret_flistpp = NULL;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
	 * Insanity check.
	 ***********************************************************/
	if (opcode != PCM_OP_C_BILL_SET_PAYINFO) {
		pin_set_err(ebufp, PIN_ERRLOC_FM,
			PIN_ERRCLASS_SYSTEM_DETERMINATE,
			PIN_ERR_BAD_OPCODE, 0, 0, opcode);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"bad opcode in op_c_bill_set_payinfo", ebufp);
		return;
	}

	/***********************************************************
	 * Debug: What did we get?
	 ***********************************************************/
	PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
		"op_c_bill_set_payinfo input flist", in_flistp);

	/***********************************************************
	 * Call main function to do it
	 ***********************************************************/
	fm_c_bill_set_payinfo(ctxp, flags, in_flistp, &r_flistp, ebufp);

	/***********************************************************
	 * Results.
	 ***********************************************************/
	if (PIN_ERR_IS_ERR(ebufp)) {
		*ret_flistpp = (pin_flist_t *)NULL;
		PIN_FLIST_DESTROY_EX(&r_flistp, NULL);
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"op_c_bill_set_payinfo error", ebufp);
	} else {
		*ret_flistpp = r_flistp;
		PIN_ERR_CLEAR_ERR(ebufp);
		PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_DEBUG,
			"op_c_bill_set_payinfo return flist", r_flistp);
	}

	return;
}

/*******************************************************************
 * fm_c_bill_set_payinfo()
 *
 *	Prep the payinfo to be ready for on-line registration.
 *
 *******************************************************************/
static void
fm_c_bill_set_payinfo(
	pcm_context_t		*ctxp,
	u_int			flags,
	pin_flist_t		*in_flistp,
	pin_flist_t		**out_flistpp,
	pin_errbuf_t		*ebufp)
{
	void            *vp = NULL;
	pin_flist_t     *flistp = NULL;
	pin_flist_t     *ret_flistp = NULL;
	pin_flist_t     *inv_flistp = NULL;
	pin_flist_t     *i_flistp = NULL;
    pin_flist_t     *b_flistp = NULL;	
	pin_flist_t		*debtor_flistp = NULL;
	pin_flist_t     *pay_dueIn_flist = NULL;
	pin_flist_t     *pay_dueOut_flist = NULL;
	pin_flist_t     *bi_i_flistp = NULL;
	pin_flist_t     *bi_o_flistp = NULL;
	poid_t          *pdp = NULL;
	poid_t          *pi_pdp = NULL;
	poid_t          *bi_pdp = NULL;
	poid_t          *ipdp = NULL;
	poid_t          *new_pdp = NULL;
	char            poid_type[256] = {0};
	char            *obj_type = NULL;
	const char  	*p_type = NULL;
	char            *payment_type = NULL;
	char            *err_msg = NULL;
        poid_t          *pay_pdp = NULL;
	u_int64         database = 0;
	u_int           *pay_type = NULL;
	u_int           *type = NULL;
	int32           option = 0;
	time_t		due_t = 0;
	u_int		due_dom = 15;
	/* The default dummy value has been changed from 0 to 1 */
	int32		dummy = 1;
	poid_t		*valp = NULL;
	int32		perr = 0;
	int32           *partialp = NULL;
	int32           *payment_term_typep = NULL;
	int32           payment_term_zero = 0;
	int32           *offsetp = NULL;
	int32           offset_default = -1;
	int32		field = 0;
	int32		is_creation = PIN_BOOLEAN_FALSE;
	int32		inv_type = 0;
	int32		in_type = 0;
	int32		inv_type_passed = PIN_BOOLEAN_FALSE;

	if (PIN_ERR_IS_ERR(ebufp))
		return;
	PIN_ERR_CLEAR_ERR(ebufp);

	/***********************************************************
         * Modify the Input Flist for formatting
         ***********************************************************/
        bi_pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp,
                        PIN_FLD_POID, 0, ebufp);
	
	if (pay_pdp && (PIN_POID_IS_TYPE_ONLY(pay_pdp))) {
		is_creation = PIN_BOOLEAN_TRUE;
	}

        partialp = (int32 *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_FLAGS,
                        1, ebufp);
        if (partialp == NULL) {
                partialp = ( int* )&dummy;
        }

        payment_type = (char *)PIN_POID_GET_TYPE(pay_pdp);

        type = (u_int *) PIN_FLIST_FLD_GET(in_flistp,
                        PIN_FLD_PAY_TYPE, 1, ebufp);

        i_flistp = ( pin_flist_t* ) PIN_FLIST_SUBSTR_GET(in_flistp,
                                        PIN_FLD_INHERITED_INFO, 1,ebufp);

        PIN_ERR_CLEAR_ERR( ebufp );
        /***********************************************************
         * Identify the bill type and then fill in the missing fields
           that are required from the database for the validation
           steps
         ***********************************************************/
		 

	/***********************************************************
	 * Create outgoing flist
	 ***********************************************************/
	*out_flistpp = PIN_FLIST_CREATE(ebufp);

	/***********************************************************
	 * Get and set the account object poid for later use.
	 ***********************************************************/
	pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, 
			PIN_FLD_ACCOUNT_OBJ, 0, ebufp);
	PIN_FLIST_FLD_SET(*out_flistpp, 
			PIN_FLD_ACCOUNT_OBJ, pdp, ebufp);

	/***********************************************************
	 * Now get the PIN_FLD_POID to continue
	 ***********************************************************/

	pdp = (poid_t *)PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_POID, 0, ebufp);
	memset(poid_type, '\0', sizeof(poid_type));

        /***********************************************************
         * Read and set the Payment term here
         **********************************************************/	       

        payment_term_typep = (int32 *)PIN_FLIST_FLD_GET(in_flistp,
				     PIN_FLD_PAYMENT_TERM, 1, ebufp);

	/***********************************************************
	 * Set payment offset value (default is -1)
	 ***********************************************************/
	offsetp = (int32 *)PIN_FLIST_FLD_GET(in_flistp,
                                     PIN_FLD_PAYMENT_OFFSET, 1, ebufp);


	// Coletar DUE_DOM
	due_dom = (u_int) PIN_FLIST_FLD_GET(in_flistp, PIN_FLD_DUE_DOM, 1, ebufp);
	
		
	PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_POID, pdp, ebufp);
		
	// Modificando o DUE_DOM
	
	if(due_dom){
		
		bi_i_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_POID, (void *) bi_pdp, ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_PAYINFO_OBJ, (void *) NULL, 
									ebufp);
		PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, bi_i_flistp, &bi_o_flistp, ebufp);

		pi_pdp = (poid_t *)PIN_FLIST_FLD_GET(bi_o_flistp, PIN_FLD_PAYINFO_OBJ, 1,
										ebufp);
										
		/*******************************************************
		 * Read the Offset value for the given payinfo POID.
		 ***********************************************************/
		pay_dueIn_flist = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_POID, (void *) pi_pdp, ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_DUE_DOM, (void *) due_dom, ebufp);
		
		PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, pay_dueIn_flist, &pay_dueOut_flist, ebufp);
		
		if(PIN_ERR_IS_ERR(ebufp)){
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_ERROR, "Deu ruim", pay_dueOut_flist);
		}
		
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_DUE_DOM, (void *)due_dom, ebufp);
		PIN_FLIST_DESTROY_EX(&pay_dueIn_flist, NULL);
		PIN_FLIST_DESTROY_EX(&pay_dueOut_flist, NULL);
	}
	
	if(offsetp){
		bi_i_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_POID, (void *) bi_pdp, ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_PAYINFO_OBJ, (void *) NULL, 
									ebufp);
		PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, bi_i_flistp, &bi_o_flistp, ebufp);

		pi_pdp = (poid_t *)PIN_FLIST_FLD_GET(bi_o_flistp, PIN_FLD_PAYINFO_OBJ, 1,
										ebufp);
										
		/*******************************************************
		 * Read the Offset value for the given payinfo POID.
		 ***********************************************************/
		pay_dueIn_flist = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_POID, (void *) pi_pdp, ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_PAYMENT_OFFSET, (int32 *) offsetp, ebufp);
		
		PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, pay_dueIn_flist, &pay_dueOut_flist, ebufp);
		
		if(PIN_ERR_IS_ERR(ebufp)){
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_ERROR, "Deu ruim", pay_dueOut_flist);
		}
		
		PIN_FLIST_DESTROY_EX(&pay_dueIn_flist, NULL);
		PIN_FLIST_DESTROY_EX(&pay_dueOut_flist, NULL);
		PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_PAYMENT_OFFSET, (int32 *)offsetp, ebufp);
	}
	
	if(payment_term_typep){
		bi_i_flistp = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_POID, (void *) bi_pdp, ebufp);
		PIN_FLIST_FLD_SET(bi_i_flistp, PIN_FLD_PAYINFO_OBJ, (void *) NULL, 
									ebufp);
		PCM_OP(ctxp, PCM_OP_READ_FLDS, 0, bi_i_flistp, &bi_o_flistp, ebufp);

		pi_pdp = (poid_t *)PIN_FLIST_FLD_GET(bi_o_flistp, PIN_FLD_PAYINFO_OBJ, 1,
										ebufp);
										
		/*******************************************************
		 * Read the Offset value for the given payinfo POID.
		 ***********************************************************/
		pay_dueIn_flist = PIN_FLIST_CREATE(ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_POID, (void *) pi_pdp, ebufp);
		PIN_FLIST_FLD_SET(pay_dueIn_flist, PIN_FLD_PAYMENT_TERM, (int32 *) payment_term_typep, ebufp);
		
		PCM_OP(ctxp, PCM_OP_WRITE_FLDS, 0, pay_dueIn_flist, &pay_dueOut_flist, ebufp);
		
		if(PIN_ERR_IS_ERR(ebufp)){
			PIN_ERR_LOG_FLIST(PIN_ERR_LEVEL_ERROR, "Deu ruim", pay_dueOut_flist);
		}
		
		PIN_FLIST_DESTROY_EX(&pay_dueIn_flist, NULL);
		PIN_FLIST_DESTROY_EX(&pay_dueOut_flist, NULL);
		
		if (payment_term_typep){
	        PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_PAYMENT_TERM,
			  (int32 *)payment_term_typep, ebufp);
		} else {

				PIN_FLIST_FLD_SET(*out_flistpp, PIN_FLD_PAYMENT_TERM,
				  (int32 *)&payment_term_zero, ebufp);
		}
	}
	



checkout:
	/*
	 * Error?
	 */
	if (PIN_ERR_IS_ERR(ebufp)) {
		PIN_ERR_LOG_EBUF(PIN_ERR_LEVEL_ERROR,
			"fm_c_bill_set_payinfo error", ebufp);
	}
	return;
}
