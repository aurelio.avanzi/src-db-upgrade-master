# db-upgrade

O módulo DB-upgrade é responsável por armazenar os objetos de upgrade do banco de dados do BRM de forma ordenada. Por meio da estrutura de diretórios deste repositório, de alguns arquivos de configuração e de uma automação bem definida, conseguimos fazer migrações de banco de dados de forma simples e segura.

## Componente de Implantação

Na arquitetura do BRM, o componente responsável por fazer a implantação dos objetos de banco de dados é o JOB config-jobs. Este job é contêiner que dispôe de um conjunto de executáveis que tem a função de processar tipos diferentes de arquivos. Por exemplo, o executável `pin_deploy` processa arquivos do tipo .podl.

O código fonte deste componente fica localizado no repositório [config-jobs](http://gitlab.redecorp.br/brm/src/config-jobs).

## Estrutura de diretórios

O db-upgrade utiliza a seguinte estrutura de diretórios:  

```bash
db_upgrade/  
├── x.y.z
│   ├── arquivo_01.podl
│   ├── arquivo_02.nap
│   ├── arquivo_03.sh
│   └── loadme.sh
├── x.y.z
│   ├── arquivo_outro.sql
│   └── loadme.sh
└── index.txt
```

Onde:

- **db_upgrade**: Diretório raiz das configurações. Todo seu conteúdo será empacotado (compactado) pelos pipelines DevOps.
- **x.y.z**: Versão que delimita um pacote de objetos de banco que devem ser implantados. x, y, e z são números inteiros não negativos, por exemplo, `1.2.3`. O time de desenvolvimento define as quebras de pacotes em versões diferentes conforme a necessidade.
- **arquivos (podl, nap, sh, sql)**: Arquivos de configuração de banco de dados. Cada tipo de arquivo tem seu próprio formato. A construção destes arquivos é responsabilidade do time de desenvolvimento.
- **loadme.sh**: Script que executa os outros arquivos. Ele é responsável pela lógica de implantação do pacote da versão `x.y.z`. Cada versão precisa ter um arquivo `loadme.sh`. A construção deste arquivo é responsabilidade do time de desenvolvimento.
- **index.txt**: Vide seção [Detalhes do arquivo `index.txt`](#detalhes-do-arquivo-indextxt)

### Detalhes do arquivo `index.txt`

O arquivo `index.txt` armazena configurações que definem a sequência de upgrades entre versões. Por exemplo, ele pode definir que a versão seguinte à `0.1.0` é a versão `0.2.0`. A construção deste arquivo é responsabilidade do time de desenvolvimento.

O preenchimento deste arquivo é responsabilidade do time de desenvolvimento. Sempre que uma nova versão for criada, uma nova linha deve ser inserida no arquivo para indicar a sequência do upgrade de versões.

O conteúdo do arquivo apresenta um formato simples:

- Cada linha do arquivo representa uma sequência de upgrade no formato `upgrade_x.y.z_to_a.b.c`, onde `x.y.z` é versão anterior e `a.b.c` é a versão posterior.
- As linhas do arquivo também são sequenciais, ou seja, a segunda linha define o upgrade seguinte à primeira linha e assim por diante.
- A primeira linha do arquivo define a primeira versão a ser implantada. Convenciona-se que a versão `0.0.0` não possui código para implantar.
- A última linha do arquivo define a última versão que deve ser implantada.

Exemplo:

```txt
upgrade_0.0.0_to_0.1.0
upgrade_0.1.0_to_0.2.0
upgrade_0.2.0_to_0.2.1
upgrade_0.2.1_to_0.3.0
upgrade_0.3.0_to_0.3.1
upgrade_0.3.1_to_0.3.2
```

> **Não é recomendado alterar sequências já definidas. Alterações deste tipo podem quebrar a integridade dos objetos de banco entre os ambientes.**

## Tabelas de controle no banco de dados

Cada banco de dados do BRM deve ter uma tabela de controle para indicar a última versão implantada no banco de dados. É por meio desta tabela que o processo automatizado vai saber quais versões precisam ser implantadas em cada banco de dados.

Seguem dados da tabela de controle:

PIN.CFG_VERSAO_HIST_T: Armazena histórico de upgrades implantados no banco de dados. Todas as implantações automáticas devem ser armazenadas nesta tabela. Deve ser usada para fins de auditoria. O registro de maior sequencial representa a última versão implantada.

- ID NUMBER(*,0): Sequencial (PK)
- VERSAO VARCHAR2(60 BYTE): Versão da implantação.
- CRIADO_EM DATE DEFAULT SYSDATE: Data da execução.

## Fluxo de processamento automatizado

A figura abaixo apresenta o fluxo de processamento de migrações.

1. Processo faz download dos artefatos deste repositório que estão armazenados no Nexus.
2. Consulta tabela de controle no banco de dados para identificar a última versão implantada.
3. Consulta arquivo `index.txt` para identificar se há uma nova versão para ser executada. Caso não haja uma nova versão, o processamento finaliza com sucesso.
4. Casa haja uma nova versão para ser implantada, o processo utiliza os artefatos baixados no passo 1 para fazer implantação. A implantação acontece por meio da execução do script `loadme.sh` que consta na pasta desta versão.
5. Caso a execução do `loadme.sh` finalize com erro, o processo esceve o erro em log e finaliza o processmento com status de erro.
6. Caso a execução do `loadme.sh` finalize com sucesso, o processo atualiza a tabela de controle para indicar a nova versão que foi executada.
7. O processo volta para o passo 2, formando um loop que só finaliza quando todas as versões estiverem sido executadas com sucesso (passo 3) ou caso aconteça algum erro no processamento (passo 5).

![Fluxo de processamento](assets/png/fluxo-processamento.png "Fluxo de processamento")

> Em caso de erros no processamento, o time responsável deve corrigir a causa raiz, ajustar os scripts de banco deste repositório (se for o caso) e realizar nova implantação. O processo automático vai iniciar o processamento a partir da versão que parou da última vez.